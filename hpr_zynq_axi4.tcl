# /usr/groups/ecad/xilinx/vivado_sdk_installed/Vivado/2015.1/bin/vivado -source ~/tcl-xilinx-script1.tcl -mode batch


set TOPNAME HPR_ZYNQ_AXI4_TOPLEVEL

#STEP#1: define the output directory area.
#
set outputDir /tmp/cbg-hpr-ksubs4a-zynq
set dropbox_dir $env(HOME)/Dropbox


# Adjust the following setting for the name of the folder containing the cbg-hpr-ksubs4a-zynq distro
set cbg_hpr_zynq_axi4_dir $env(HOME)/vivado-cmdlines/cbg-hpr-ksubs4a-zynq


# We do not need to copy all the distro files into every work folder. So we use a separate design_dr for work.
# Adjust this to our work folder (will initially be the same as the distro)
set design_dir $env(HOME)/vivado-cmdlines/cbg-hpr-ksubs4a-zynq

# Kiwi HLS library of FUs needed for Kiwi designs.
set kiwi_kdistro $env(HPRLS)/kiwipro/kiwic/distro
set kiwi_fu_dir  $kiwi_kdistro/lib/hpr_ipblocks
set cvtech1      $kiwi_fu_dir/cvip0/cvgates.v
set cvtech2      $kiwi_fu_dir/cvip0/cv_fparith.v

#set cvtech2 $dropbox_dir/cv_fparith.v


file mkdir $outputDir


#--------------------
# STEP#2: setup design sources and constraints
#

read_verilog $cvtech1 $cvtech2 





# These are the RTL files needed for this particular ksubs4a wrapper.
read_verilog $design_dir/HPR_ZYNQ_AXI4_TOPLEVEL.v 
read_verilog $cbg_hpr_zynq_axi4_dir/cbg-hpr-ksubs4a-rtl/hpr_axi4_pio_target.v
read_verilog $cbg_hpr_zynq_axi4_dir/cbg-hpr-ksubs4a-rtl/hpr_zynq_axi4_gp0_ms_bondout.v
read_verilog $cbg_hpr_zynq_axi4_dir/cbg-hpr-ksubs4a-rtl/hpr_hfast_to_axi4_master.v
read_verilog $cbg_hpr_zynq_axi4_dir/xilinx-ip-folder2/processing_system7_v5_5_processing_system7.v

# XADC interface for joysticks
#read_verilog  $joystick_dir/xadc_module.v
#read_verilog  $joystick_dir/JoyStickUnit.v


# Inter-blade link for multi-FPGA designs
#read_verilog $dropbox_dir/myserdes/DuplexSerialLink.v


# Framestore
#read_verilog $dropbox_dir/myserdes/myserdes.v
#read_verilog $dropbox_dir/rtl-misc/FSTORE_CLEANER.v
#read_verilog  $design_dir/hdmi_fstore.v
#read_verilog  $design_dir/serdes10.v
#read_verilog  $design_dir/tmds_encode.v
#read_verilog /home/djg11/filer/verilog/framestores/rachelset.v
#read_verilog /home/djg11/filer/verilog/framestores/fstore2.v


# This is a memory bank made up of BRAM, perhaps used in some designs.
# read_verilog $design_dir/membank_hf1.v


# These are the (Kiwi-generated) RTL files for the current design - you should be importing your own
# This is the design of interest: edit this line to refer to your 'Role' in the Shell/Roll paradigm. You should take your own copy of smalldut.v 
# read_verilog $design_dir/hpr_zynq_axi4_smalldut.v
read_verilog $dropbox_dir/Kiwi-posit-babar-cordic/kiwibuild0/cordic_test.v


# These are the RTL files for the Xilinx protocol convertor
# source "$cbg_hpr_zynq_axi4_dir/axi3-simple-ipblocks.tcl"
# read_verilog $cbg_hpr_zynq_axi4_dir/xilinx_ip/djgaxi.v


# WARNING: [Synth 8-3331] design processing_system7_v5_5_processing_system7 has unconnected port FTMD_TRACEIN_DATA[25]
# INFO: [Common 17-14] Message 'Synth 8-3331' appears 100 times and further instances of the messages will be disabled. Use the Tcl command set_msg_config to change the current settings.
# set_msg_config -id "Synth 8-11" -limit 500"); this will increase the message limit for this warning from 100 to 500.
# In Vivado 2012.4 and earlier, use the set_msg_limit TCL command (e.g., "set_msg_limit -id "Synth 8-11" 500").
set_msg_config -id "Synth 8-3331" -limit 20000
set_msg_config -id "Synth 8-3332" -limit 20000
set_msg_config -id "Synth 8-3333" -limit 20000

#
#--------------------
# STEP#3: run synthesis, write design checkpoint, report timing,
# and utilization estimates
#

# Set the Zynq Chip type
# Parallella wants: set part xc7z010clg400-1  set vdefine PARCARD10=1
# Zedboard wants:   set part xc7z020clg484-1  set vdefine ZEDBOARD20=1
# Pynq wants        set part xc7z020clg400-1  set vdefine PYNQ20=1
# vc707 wants synth_design -top fstop -part xc7vx485tffg1761-2

# set part xc7z020clg484-1  
# set vdefine ZEDBOARD20=1

#set part xc7z010clg400-1  
#set vdefine PARCARD10=1

set part xc7z020clg400-1  
set vdefine PYNQ20=1

# Set the PCB pinout
# Parallella wants: set pinout $cbg_hpr_zynq_axi4_dir/pinouts/parallella10.xdc
# Zedboard wants:   set pinout $cbg_hpr_zynq_axi4_dir/pinouts/zedboard20.xdc
# Pynq wants:       set pinout $cbg_hpr_zynq_axi4_dir/pinouts/PYNQ-Z1_C.xdc

#set pinout $cbg_hpr_zynq_axi4_dir/zynq_pinouts/zedboard20.xdc
#set pinout $cbg_hpr_zynq_axi4_dir/zynq_pinouts/parallella10.xdc
set pinout $cbg_hpr_zynq_axi4_dir/zynq_pinouts/PYNQ-Z1_C.xdc
#read_xdc $cbg_hpr_zynq_axi4_dir/zynq_pinouts/PYNQ-Z1_HDMI_TX.xdc


synth_design -top $TOPNAME -part $part -verilog_define $vdefine 
# -include_dirs $RTL_INCLUDE_PATH 
# Constraints - xcf file
read_xdc $pinout
# read_xdc extra.xdc


#write_checkpoint -force $outputDir/post_synth.dcp
#report_timing_summary -datasheet -file $outputDir/post_synth_timing_summary.rpt
report_utilization -file $outputDir/post_synth_util.rpt

# Run custom script to report critical timing paths
# reportCriticalPaths $outputDir/post_synth_critpath_report.csv


#--------------------
# STEP#4: run logic optimization, placement and physical logic optimization,
# write design checkpoint, report utilization and timing estimates
#
opt_design
#reportCriticalPaths $outputDir/post_opt_critpath_report.csv
place_design
report_clock_utilization -file $outputDir/clock_util.rpt
#

# Optionally run optimization if there are timing violations after placement
#   if {[get_property SLACK [get_timing_paths -max_paths 1 -nworst 1 -setup]] < 0} {
#   puts "Found setup timing violations => running physical optimization"
#   phys_opt_design
#   }

#write_checkpoint -force $outputDir/post_place.dcp
report_utilization -file $outputDir/post_place_util.rpt

#report_timing_summary -file $outputDir/post_place_timing_summary.rpt

#--------------------
# STEP#5: run the router, write the post-route design checkpoint, report the routing
# status, report timing, power, and DRC, and finally save the Verilog netlist.
#
route_design

#write_checkpoint -force $outputDir/post_route.dcp
report_route_status -file $outputDir/post_route_status.rpt

# ACLK is 100 MHz at the moment. 
#create_clock -period 10.000 -name my_main_clock -waveform {0.000 5.000} [get_nets zynq_axi_master_i/processing_system7_0/inst/PS7_i/FCLKCLK[0]]
# create_clock -period 10.000 -name my_main_clock -waveform {0.000 5.000} [get_nets    the_ps7/the_ps7_v5_5/PS7_i/FCLKCLK[0]]
create_clock -period 10.000 -name my_main_clock -waveform {0.000 5.000} [get_nets   BondOut_FCLK_CLK0]
report_timing_summary -file $outputDir/post_route_timing_summary.rpt -report_unconstrained




report_power -file $outputDir/post_route_power.rpt
#report_drc -file $outputDir/post_imp_drc.rpt
#write_verilog -force $outputDir/topfpga_impl_netlist.v -mode timesim -sdf_anno true
#
# STEP#6: generate a bitstream
#
write_bitstream -force $outputDir/topfpga.bit

# report_property [get_ports hdmi_tx_clk_p ]

# eof

