# HPR L/S and Kiwi Scientific Acceleration
# (C) 2018 D J Greaves, University of Cambridge, Computer Laboratory.

# Kiwi substrate (aka shell in Shell/Role paradigm).  Used for single designs on a single FPGA blade.
# For more-complex systems use HPR System Integrator (when it starts working!).
 
This is      https://bitbucket.org/djg11/cbg-hpr-ksubs4a-zynq
Described on http://www.cl.cam.ac.uk/research/srg/han/hprls/orangepath/kiwic-demos/zynq-pio-dma/
See also     http://koo.corpus.cam.ac.uk/kiwic-download/

Ksubs4a for zynq platform.

This version is under development April 2018.  The above web site will be updated with specific ksubs4 instructions at this time.



 - This version does not have virtual circuits for the Noc16.
 - This version may not (does not currently) have the C server code - get that from the https://bitbucket.org/djg11/kiwi-ksubs3 repo which should be compatible.


# Kiwi Scientific Acceleration: Substrate Code
#
# (C) 2016 DJ Greaves, University of Cambridge, Computer Laboratory.
#
#


The ksubs3 features included:

    8 hardware LEDs and switches for very-low level I/O. These may not all be present on certain blades, such as the Parallella.

    Programmed I/O register file mapped into ARM address space.

    File system access is via the Kiwi network-on-chip Noc16, which can be chained through any number of components.

    Abend syndrome reporting (shows running status or reason for stopping).

    Design serial number (simple confidence check that correct design is loaded in the FPGA).

    Hardware waypoint (a method of reporting how far through an application execution has reached).

Missing -    Main-thread PC monitor.

    64-bit simple result register (this is the easiest form of output to use to avoid a simple application from being stripped entirely during trimming under FPGA-vendor logic synthesis)

    Run/stop control via clock-enable input or other.

    misc_reg0 and misc_mon0 - 32-bit general purpose PIO registers.



Instructions for use

   The instructions are missing at the moment.  Essentially you discard hpr_zynq_axi4_smalldut.v and replace it with the Kiwi-generated code.


Notes:

The design emitted by KiwiC needs to be inserted in the substrate for the appropriate board or server blade.

The main substrate shim is boiler-plate RTL code that connects to the M_AXI_GPO memory-mapped I/O bus for simple start/stop control and parameter exchange. It is recommended that every design compiled has a serial number hard-coded in the C# source code and that this is modified on every design iteration. The first function of the substrate shim is to provide readback of this value.

The other features of the shim are starting and stopping the design and collecting abend codes. Sources of abend are null-pointer de-reference, out-of-memory, divide-by-zero, user assertion failure, and so on.

A Kiwi design that makes access to main memory will have a number of load/store ports. These can be half-duplex or simplex. Simplex is preferred when main memory is served over the AXI bus, as in the Zynq design. (Of course there may be a lot of BRAM memory in the synthesised design itself, but that does not appear on this figure.) Simplex works well with AXI since each AXI port itself consists of two independent simplex ports, one for reading and one for writing.

In the illustrated example, the design used three simplex load/store ports. These need connecting to the available AXI busses hardened on the Zynq platform and made available to the FPGA programmable logic. The user has the choice of a cache-coherent, 64-bit AXI bus that will compete with the ARM cores for the L2 cache front-side bandwidth, or four other high-performance 64-bit AXI busses that offer high DRAM bandwidth. These four are not used in the example figure.

Each KiwiC-generated load-store port is an in-order unit, like an individual load or store station in an out-of-order processor. By multiplexing their traffic onto AXI-4 busses, bus bandwidths are matched and out-of-order service from the DRAM system is exploited.

Each load/store port in the generated RTL has is properly described in the IP-XACT rendered by KiwiC that describes the resulting design. When this IP-XACT is imported into a design suite, manual wiring of the load/store ports to the AXI switch ports can be done in a schematic editor. (Soon this will be automated when KiwiC invokes System Integrator tool inside the HPR L/S library). 





For general and simple use with Kiwi, you should write a main function that looks a little like this
(note runstop needs further work and will be changed soon).
  
  // These are the standard net definitions for ksubs4a without a shim.
  [Kiwi.OutputBitPort("design_serial_number")] static int design_serial_number = 0x11FF01; // Change manually on every edit.
  [Kiwi.InputWordPort("ksubs_runstop")] static byte g_ksubs_runstop;

  [Kiwi.InputWordPort("ksubsGpioSwitches")] static byte g_ksubsGpioSwitches;

  // Leds in Kiwi.dll, not here.
  //[Kiwi.OutputWordPort("ksubsGpioLeds")] static uint g_ksubsGpioLeds;

  [Kiwi.InputWordPort("pio_addr")] static uint g_pio_addr;
  [Kiwi.InputWordPort("pio_wdata")] static uint g_pio_wdata;
  [Kiwi.OutputWordPort("pio_rdata")] static uint g_pio_rdata = 101;   // A simple tie off. TODO show the proper code for PIO slave please.
  [Kiwi.InputWordPort("pio_hwen")] static uint g_pio_hwen;

  [Kiwi.InputWordPort("misc_reg0")] static uint g_misc_reg0;
  [Kiwi.InputWordPort("misc_mon0")] static uint g_misc_mon0;
  [Kiwi.OutputWordPort("result_lo")] static uint g_result_lo;
  [Kiwi.OutputWordPort("result_hi")] static uint g_result_hi;

  [Kiwi.HardwareEntryPoint()]
  static public int Main()
  {
    Console.WriteLine("Kiwi Testbench0 start.");
    int ans1 = 100;
    int ans0 = RunTestFib(out ans1);
    g_result_lo = (uint)ans0;
    g_result_hi = (uint)ans1;
    Console.WriteLine("Kiwi Testbench0 finish.  Answer {0} {1}", ans0, ans1);
    Kiwi.ReportNormalCompletion();
    return 0;
 }




// eof