// Just a place holder

// HPR_ZYNQ_AXI4_SMALLDUT

module DIT
#(
    parameter integer DMA_DATA_WIDTH = 32,
    parameter integer DMA_ADDR_WIDTH = 32
)
(
	   input 		       clk,
	   input 		       reset,
	   output [7:0] 	       unary_gpio_leds,

  
 // Kiwi Ksubs4 Standard Busses
	   output [23:0] 	       design_serial_number, 
	   input [1:0] 		       ksubs_runstop,        // Single-step and reset control
	   input [7:0] 		       ksubsGpioSwitches,    // Physical switches on the FPGA blade
	   output [7:0] 	       ksubsGpioLeds,        // Leds on the blade
	   output [31:0] 	       misc_mon0,            // General purpose application-specific 32-bit Role output for PIO reading
	   input [31:0] 	       misc_reg0,            // General purpose application-specific 32-bit bus - PIO output to Role
	   output [31:0] 	       result_lo, result_hi, // Further Role output for PIO reading (like misc_mon) but normally used for simple computation result.
	   output [7:0] 	       ksubsAbendSyndrome,   // Kiwi Run/Stop indication and runtime error or exit code reporting.
	   output [7:0] 	       ksubsManualWaypoint,  // Waypoint indication 

 
 // Slotted ring - slots have 72 bits in them. Any station can write to a slot whose top
 // byte is zero, which denotes not in use.  Stations should pass on all other slots
 // unchanged unless they are responding to a command addressed to themselves.
 
 // Ksubs Noc16 duplex interface: Net directions are input on RX and output on TX.
	   input [63:0] 	       Ksubs3_Noc16_RxData_lo,
 //input [63:0] Ksubs3_Noc16_RxData_hi,
	   input [7:0] 		       Ksubs3_Noc16_RxData_cmd,
	   input 		       Ksubs3_Noc16_RxData_valid,
	   output 		       Ksubs3_Noc16_RxData_rdy,


	   output [63:0] 	       Ksubs3_Noc16_TxData_lo,
 //output [63:0] 	Ksubs3_Noc16_TxData_hi,
	   output [7:0] 	       Ksubs3_Noc16_TxData_cmd,
	   output 		       Ksubs3_Noc16_TxData_valid,
	   input 		       Ksubs3_Noc16_TxData_rdy,
 

 
// HFAST1 PORT MASTER
	   output [DMA_ADDR_WIDTH-1:0] hfast_addr,
	   output [DMA_DATA_WIDTH-1:0] hfast_wdata,
	   input [DMA_DATA_WIDTH-1:0]  hfast_rdata, 
	   output 		       hfast_req,
	   output 		       hfast_rwbar, 
	   input 		       hfast_ack, 




 // Programmed I/O connections
	   input 		       pio_hwen, 
	   output [31:0] 	       pio_rdata,
	   input [31:0] 	       pio_wdata,
	   input [10:0] 	       pio_addr


	   );

   reg [31:0] 		addr_reg, r2;
   reg [11:0] 		c0, c1, c2;
   reg 			 t0, t1;
   reg 			 rd_cmd, wr_cmd;
   
   assign hfast_addr = addr_reg;
   assign hfast_wdata = pio_wdata;
   assign hfast_req = rd_cmd || wr_cmd;
   assign hfast_rwbar = !wr_cmd;

   always@(posedge clk)
       if (reset) begin
	  addr_reg <= 1;
	  r2 <= 0;
	  end
	  else 
	    begin
	       rd_cmd <= 0;
	       wr_cmd <= 0;
	       
	       if (pio_hwen && pio_addr == 11'h40) begin
		  addr_reg <= pio_wdata;
	       end


	       if (pio_hwen && pio_addr == 11'h44 && !rd_cmd) begin
		  rd_cmd <= 1;
	       end


	       if (pio_hwen && pio_addr == 11'h48 && !wr_cmd) begin
		  wr_cmd <= 1;
	       end

	       if (pio_hwen && pio_addr == 11'h50 && !wr_cmd) begin
		  r2 <= pio_wdata;
	       end

	       // Divide down by 10^10 to make an LED flash nicely.
	  c0 <= (t0) ? 9999: c0 - 1;
	  t0 <= !c0;
	  if (t0) c1 <= (c1 == 9999) ?0: c1+1;
	  t1 <= !c1;
	  if (t0 & t1) c2 <= c2 + 1;
       end

   assign ksubsGpioLeds = c2;

   assign pio_rdata = 
		      (pio_addr == 11'h40) ? addr_reg: 
   		      (pio_addr == 11'h48) ? hfast_rdata:
   		      (pio_addr == 11'h50) ? r2:		      
		      {hfast_ack, 8'hc0};
   
   assign design_serial_number = 16'h2055; 
   assign ksubsManualWaypoint = 50;// This should normally go up as the computation makes progress.
   assign ksubsAbendSyndrome = 1;
   // Some simple logic as a demo application
   assign result_lo = misc_reg0 + 1;
   assign result_hi = misc_reg0;

   
   // As there is no ring station in this toy design we connect the output directly to thexs input.
   assign Ksubs3_Noc16_TxData_lo = Ksubs3_Noc16_RxData_lo; 
   assign Ksubs3_Noc16_TxData_cmd = Ksubs3_Noc16_RxData_cmd;
   assign Ksubs3_Noc16_TxData_valid = Ksubs3_Noc16_RxData_valid;
   assign Ksubs3_Noc16_RxData_rdy = Ksubs3_Noc16_TxData_rdy; // Not rdy is assigned in the reverse direction to the data.

   
endmodule // DUT
// eof

