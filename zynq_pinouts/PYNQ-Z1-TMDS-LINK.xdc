
## PMOD pinout
##   3  0  D3 D2 D1 D0          3  0  I1- I1+ I0- I0+
##   -----------------          ---------------------
##   3  0  D7 D6 D5 D4          3  0  O1- O1+ O0- O0+


set_property -dict { PACKAGE_PIN Y18   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_in_p[0] }]; 
set_property -dict { PACKAGE_PIN Y19   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_in_n[0] }]; 
set_property -dict { PACKAGE_PIN Y16   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_in_p[1] }]; 
set_property -dict { PACKAGE_PIN Y17   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_in_n[1] }]; 

set_property -dict { PACKAGE_PIN U18   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_out_p[0] }]; 
set_property -dict { PACKAGE_PIN U19   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_out_n[0] }]; 
set_property -dict { PACKAGE_PIN W18   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_out_p[1] }]; 
set_property -dict { PACKAGE_PIN W19   IOSTANDARD TMDS_33 } [get_ports { pmod_ja_diff_out_n[1] }]; 
