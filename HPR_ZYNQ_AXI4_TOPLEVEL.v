`timescale 1ns / 1ps

// `default_nettype none

// HPR L/S and Kiwi Scientific Acceleration
// (C) 2015-18 D J Greaves, University of Cambridge, Computer Laboratory.
// Kiwi substrate (aka shell in Shell/Role paradigm).  Used for single designs on a single FPGA blade.
// For more-complex systems use HPR System Integrator (when it starts working!).



module HPR_ZYNQ_AXI4_TOPLEVEL
   (
`ifdef PARCARD10
    output GPIO0_N, output GPIO0_P,
`endif

`ifdef ZEDBOARD20
    input wire  GCLK_in /*33 MHz Y9*/,
    input wire  BTNC, BTND, BTNL, BTNR, BTNU,
    input wire  USER_SW7, input wire USER_SW6, input wire USER_SW5, input wire USER_SW4,
    input wire  USER_SW3, input wire USER_SW2, input wire USER_SW1, input wire USER_SW0,
    output USER_LD7, output USER_LD6, output USER_LD5, output USER_LD4,
    output USER_LD3, output USER_LD2, output USER_LD1, output USER_LD0,
`endif
    
`ifdef PYNQ20
    output wire [7:0] pmod_jb,


    output wire rgb_led4_b,
    output wire rgb_led4_g,
    output wire rgb_led4_r,    
    
    output wire rgb_led5_b,
    output wire rgb_led5_g,
    output wire rgb_led5_r,    

    output wire [3:0] led,
    input wire [3:0] btn,
    input wire sysclk125,

    input wire [1:0] pynq_sw,
    
    //ADC
    input wire [5:0] ck_an_n,
    input wire [5:0] ck_an_p,    
    //input wire [23:20] ck_io,

`ifdef ARTY_SERIAL_LINK
    input [1:0] pmod_ja_diff_in_p, // Serial link to other Arty-Z7 card.
    input [1:0] pmod_ja_diff_in_n,
    output [1:0] pmod_ja_diff_out_p,
    output [1:0] pmod_ja_diff_out_n,
`endif

`ifdef HDMI_OUTPUT    
    // HDMI
    output wire [2:0] hdmi_tx_d_n,
    output wire [2:0] hdmi_tx_d_p,
    output wire hdmi_tx_clk_n,
    output wire hdmi_tx_clk_p,
    input wire hdmi_tx_hpdn,
`endif
    
`endif

    inout wire [53:0]FIXED_IO_mio
    );
   

   
`ifdef PARCARD10
   // Parcard-djg3 has an LED soldered to GPIO0 - drive both N and P in parallel please.
   assign GPIO0_N = ksubsGpioLeds[0];
   assign GPIO0_P = ksubsGpioLeds[0];
`endif

`ifdef ZEDBOARD20
   wire [7:0] 	     user_sliders = { USER_SW7, USER_SW6, USER_SW5, USER_SW4, USER_SW3, USER_SW2, USER_SW1, USER_SW0 };
   assign { USER_LD7, USER_LD6, USER_LD5, USER_LD4, USER_LD3, USER_LD2, USER_LD1, USER_LD0 } = ksubsGpioLeds[7:0];
`endif

   // Ksubs4 Standard Busses
   wire [23:0] 	     design_serial_number; 
   wire [1:0] 	     ksubs_runstop;               // Single-step and reset control
   wire [7:0] 	     ksubsGpioSwitches;           // Physical switches on the FPGA blade
   wire [7:0] 	     ksubsGpioLeds;               // Leds on the blade
   wire [31:0] 	     misc_mon0;                   // General purpose application-specific 32-bit Role output for PIO reading
   wire [31:0] 	     misc_reg0;                   // General purpose application-specific 32-bit bus - PIO output to Role
   wire [31:0] 	     result_lo, result_hi;        // Further role output for PIO reading (like misc_mon) but normally used for simple computation result.
   wire [7:0] 	     ksubsAbendSyndrome;          // Kiwi Run/Stop indication and runtime error or exit code reporting.
   wire [7:0] 	     ksubsManualWaypoint;         // Waypoint indication 


   
`ifdef PYNQ20
   assign ksubsGpioSwitches = { 4'd0, btn[3:0] };
   assign led[3:0] = ksubsGpioLeds[3:0];
			    
	     
`endif
 

   wire BondOut_FCLK_CLK0;
   wire BondOut_FCLK_RESET0_N;


   // Ksubs Noc16 slotted ring - slots have 72 bits in them. Any station can write to a slot whose top
   // byte is zero, which denotes not in use.  Stations should pass on all other slots
   // unchanged unless they are responding to a command addressed to themselves.
   // Note the Rx Signals of one station should be connected to the TX signals of the next station owing to the ring network configuration.
   wire [63:0] 		    Ksubs3_Noc16_Arc0_Data_lo;
   //wire [63:0] 		    Ksubs3_Noc16_Arc0_Data_hi;
   wire [7:0] 		    Ksubs3_Noc16_Arc0_Data_cmd;
   wire 		    Ksubs3_Noc16_Arc0_Data_valid;
   wire 		    Ksubs3_Noc16_Arc0_Data_rdy;
   
   wire [63:0] 		    Ksubs3_Noc16_Arc1_Data_lo;
   //wire [63:0] 		    Ksubs3_Noc16_Arc1_Data_hi;
   wire [7:0] 		    Ksubs3_Noc16_Arc1_Data_cmd;
   wire 		    Ksubs3_Noc16_Arc1_Data_valid;
   wire 		    Ksubs3_Noc16_Arc1_Data_rdy;

   


   // Programmed I/O access to director shim
   wire [11:0] pio_addr;
   wire [31:0] pio_rdata, pio_wdata;
   wire 		    pio_hwen;





`ifdef FRAMESTORE_USED			      
   wire [7:0] 		    setget_pixel_return_client0;
   wire [31:0] 		    setget_pixel_axx_client0, setget_pixel_ayy_client0;
   wire 		    setget_pixel_readf_client0;
   wire [7:0] 		    setget_pixel_wdata_client0;
   wire 		    setget_pixel_ack_client0;
   wire 		    setget_pixel_req_client0;

   wire [7:0] 		    setget_pixel_return_client0_raw;
   wire [31:0] 		    setget_pixel_axx_client0_raw, setget_pixel_ayy_client0_raw;
   wire 		    setget_pixel_readf_client0_raw;
   wire [7:0] 		    setget_pixel_wdata_client0_raw;
   wire 		    setget_pixel_req_client0_raw;

   
   wire [7:0] 		    setget_pixel_return_client1;
   wire [31:0] 		    setget_pixel_axx_client1, setget_pixel_ayy_client1;
   wire 		    setget_pixel_readf_client1;
   wire [7:0] 		    setget_pixel_wdata_client1;
   wire 		    setget_pixel_ack_client1;
   wire 		    setget_pixel_req_client1;
   
   wire [7:0] 		    setget_pixel_return_union;
   wire [31:0] 		    setget_pixel_axx_union, setget_pixel_ayy_union;
   wire 		    setget_pixel_readf_union;
   wire [7:0] 		    setget_pixel_wdata_union;
   wire 		    setget_pixel_ack_union;
   wire 		    setget_pixel_req_union;


   
   // Crude arbiter
   assign		    setget_pixel_req_union = setget_pixel_req_client0 || setget_pixel_req_client1;
   assign		    setget_pixel_return_client0 = setget_pixel_return_union;
   assign		    setget_pixel_return_client1 = setget_pixel_return_union;   
   assign setget_pixel_axx_union = (setget_pixel_req_client0) ? 	    setget_pixel_axx_client0:  setget_pixel_axx_client1;
   assign setget_pixel_ayy_union = (setget_pixel_req_client0) ? 	    setget_pixel_ayy_client0:  setget_pixel_ayy_client1;
   assign setget_pixel_readf_union = (setget_pixel_req_client0) ? 	    setget_pixel_readf_client0:  setget_pixel_readf_client1;
   assign setget_pixel_wdata_union = (setget_pixel_req_client0) ? 	    setget_pixel_wdata_client0:  setget_pixel_wdata_client1;      
   assign		    setget_pixel_ack_client0 =  setget_pixel_ack_union; // ack them both - yuck
   assign		    setget_pixel_ack_client1 =  setget_pixel_ack_union;   
`endif //  `if FRAMESTORE_USED
   
`ifdef PYNQ20
   wire 		    harder_mode = pynq_sw[0];
`else
   wire 		    harder_mode = 0;
`endif
   
`ifdef ADC_USED   
   wire [31:0] pio_adc_rdata;   // unused currently - see ARTY JOYSTICK design.
`endif



   parameter integer 			  DMA_DATA_WIDTH = 32;
   parameter integer 			  DMA_ADDR_WIDTH = 32;


   wire [DMA_ADDR_WIDTH-1:0] 		  hfast_addr;
   wire [DMA_DATA_WIDTH-1:0] 		  hfast_wdata;
   wire [DMA_DATA_WIDTH-1:0] 		  hfast_rdata;
   wire 				  hfast_req;
   wire 				  hfast_rwbar; 
   wire 				  hfast_ack; 

   
   // The directorate shim, around the KiwiC synthesis output (or the KiwiC output directly or your own design etc..)
   DUT dut_i(
	     // Kiwi Ksubs4 Standard Busses			  
	     .design_serial_number(design_serial_number),
	     .ksubs_runstop(ksubs_runstop),
	     .ksubsGpioSwitches(ksubsGpioSwitches),	
	     .ksubsGpioLeds(ksubsGpioLeds),
	     .misc_mon0(misc_mon0),
	     .misc_reg0(misc_reg0), 				
	     .result_hi(result_hi),  
	     .result_lo(result_lo),
	     .ksubsAbendSyndrome(ksubsAbendSyndrome),
	     .ksubsManualWaypoint(ksubsManualWaypoint),				

`ifdef LOADSTORE0	     
	     // LoadStore Port 0 (in general we need more but use HPR System Integrator for those systems in the future.
	     // Note - you need to carefully manage your memory map if, as it typically is, Zynq linux is running in the same DRAM bank!
	     // Instructions for memory map management are ...

	     // This is what this shim currently supports.
	     .hfast_addr(hfast_addr),
	     .hfast_wdata(hfast_wdata),
	     .hfast_rdata(hfast_rdata),
	     .hfast_req(hfast_req),
	     .hfast_rwbar(hfast_rwbar),
	     .hfast_ack(hfast_ack),			
`endif
	     
`ifdef DRAMBANK	     
	     // This is what KiwiC currently creates
	     .dram0bank_OPREQ(dram0bank_OPREQ),
	     .dram0bank_OPRDY(dram0bank_OPRDY),
	     .dram0bank_ACK(dram0bank_ACK),
	     .dram0bank_RWBAR(dram0bank_RWBAR),
	     .dram0bank_WDATA(dram0bank_WDATA),
	     .dram0bank_ADDR(dram0bank_ADDR),
	     .dram0bank_RDATA(dram0bank_RDATA),
	     .dram0bank_LANES(dram0bank_LANES),
`endif
	     
				
`define PIO_USED 1		    
`ifdef PIO_USED
		    // Programmed I/O access to director shim
		    .pio_hwen(pio_hwen), .pio_rdata(pio_rdata), .pio_wdata(pio_wdata), .pio_addr(pio_addr),
`endif
		    
`ifdef ADC_USED
		    .pio_adc_rdata(pio_adc_rdata),   
`endif
`ifdef FRAMESTORE_USED			   
		    .setget_pixel_return(setget_pixel_return_client1),
		    .setget_pixel_axx(setget_pixel_axx_client1),
		    .setget_pixel_ayy(setget_pixel_ayy_client1),
		    .setget_pixel_readf(setget_pixel_readf_client1),
		    .setget_pixel_wdata(setget_pixel_wdata_client1),
		    .setget_pixel_ack(setget_pixel_ack_client1),
		    .setget_pixel_req(setget_pixel_req_client1),
`endif			   

		    .clk(BondOut_FCLK_CLK0),
		    .reset(!BondOut_FCLK_RESET0_N)
    );


   wire 		    clk = BondOut_FCLK_CLK0;

   //==================================================================

   parameter integer 	    C_M_AXI_GP0_THREAD_ID_WIDTH = 12;
   parameter integer 	    C_S_AXI_GP0_THREAD_ID_WIDTH = 12;



   wire [31:0] 		    BondOut_M_AXI_GP0_ARADDR;
   wire [1:0] 		    BondOut_M_AXI_GP0_ARBURST;
   wire [3:0] 		    BondOut_M_AXI_GP0_ARCACHE;
   wire [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_M_AXI_GP0_ARID;
   wire [3:0] 				  BondOut_M_AXI_GP0_ARLEN;
   wire [1:0] 				  BondOut_M_AXI_GP0_ARLOCK;
   wire [2:0] 				  BondOut_M_AXI_GP0_ARPROT;
   wire [3:0] 				  BondOut_M_AXI_GP0_ARQOS;
   wire 				  BondOut_M_AXI_GP0_ARREADY;
   wire [2:0] 				  BondOut_M_AXI_GP0_ARSIZE;
   wire 				  BondOut_M_AXI_GP0_ARVALID;
   wire [31:0] 				  BondOut_M_AXI_GP0_AWADDR;
   wire [1:0] 				  BondOut_M_AXI_GP0_AWBURST;
   wire [3:0] 				  BondOut_M_AXI_GP0_AWCACHE;
   wire [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_M_AXI_GP0_AWID;
   wire [3:0] 				  BondOut_M_AXI_GP0_AWLEN;
   wire [1:0] 				  BondOut_M_AXI_GP0_AWLOCK;
   wire [2:0] 				  BondOut_M_AXI_GP0_AWPROT;
   wire [3:0] 				  BondOut_M_AXI_GP0_AWQOS;
   wire 				  BondOut_M_AXI_GP0_AWREADY;
   wire [2:0] 				  BondOut_M_AXI_GP0_AWSIZE;
   wire 				  BondOut_M_AXI_GP0_AWVALID;
   wire [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_M_AXI_GP0_BID;
   wire 				  BondOut_M_AXI_GP0_BREADY;
   wire [1:0] 				  BondOut_M_AXI_GP0_BRESP;
   wire 				  BondOut_M_AXI_GP0_BVALID;
   wire [31:0] 				  BondOut_M_AXI_GP0_RDATA;
   wire [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_M_AXI_GP0_RID;
   wire 				  BondOut_M_AXI_GP0_RLAST;
   wire 				  BondOut_M_AXI_GP0_RREADY;
   wire [1:0] 				  BondOut_M_AXI_GP0_RRESP;
   wire 				  BondOut_M_AXI_GP0_RVALID;
   wire [31:0] 				  BondOut_M_AXI_GP0_WDATA;
   wire [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_M_AXI_GP0_WID;
   wire 				  BondOut_M_AXI_GP0_WLAST;
   wire 				  BondOut_M_AXI_GP0_WREADY;
   wire [3:0] 				  BondOut_M_AXI_GP0_WSTRB;
   wire 				  BondOut_M_AXI_GP0_WVALID;



   wire [31:0] 		    BondOut_S_AXI_GP0_ARADDR;
   wire [1:0] 		    BondOut_S_AXI_GP0_ARBURST;
   wire [3:0] 		    BondOut_S_AXI_GP0_ARCACHE;
   wire [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_S_AXI_GP0_ARID;
   wire [3:0] 				  BondOut_S_AXI_GP0_ARLEN;
   wire [1:0] 				  BondOut_S_AXI_GP0_ARLOCK;
   wire [2:0] 				  BondOut_S_AXI_GP0_ARPROT;
   wire [3:0] 				  BondOut_S_AXI_GP0_ARQOS;
   wire 				  BondOut_S_AXI_GP0_ARREADY;
   wire [2:0] 				  BondOut_S_AXI_GP0_ARSIZE;
   wire 				  BondOut_S_AXI_GP0_ARVALID;
   wire [31:0] 				  BondOut_S_AXI_GP0_AWADDR;
   wire [1:0] 				  BondOut_S_AXI_GP0_AWBURST;
   wire [3:0] 				  BondOut_S_AXI_GP0_AWCACHE;
   wire [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_S_AXI_GP0_AWID;
   wire [3:0] 				  BondOut_S_AXI_GP0_AWLEN;
   wire [1:0] 				  BondOut_S_AXI_GP0_AWLOCK;
   wire [2:0] 				  BondOut_S_AXI_GP0_AWPROT;
   wire [3:0] 				  BondOut_S_AXI_GP0_AWQOS;
   wire 				  BondOut_S_AXI_GP0_AWREADY;
   wire [2:0] 				  BondOut_S_AXI_GP0_AWSIZE;
   wire 				  BondOut_S_AXI_GP0_AWVALID;
   wire [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_S_AXI_GP0_BID;
   wire 				  BondOut_S_AXI_GP0_BREADY;
   wire [1:0] 				  BondOut_S_AXI_GP0_BRESP;
   wire 				  BondOut_S_AXI_GP0_BVALID;
   wire [31:0] 				  BondOut_S_AXI_GP0_RDATA;
   wire [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_S_AXI_GP0_RID;
   wire 				  BondOut_S_AXI_GP0_RLAST;
   wire 				  BondOut_S_AXI_GP0_RREADY;
   wire [1:0] 				  BondOut_S_AXI_GP0_RRESP;
   wire 				  BondOut_S_AXI_GP0_RVALID;
   wire [31:0] 				  BondOut_S_AXI_GP0_WDATA;
   wire [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0] BondOut_S_AXI_GP0_WID;
   wire 				  BondOut_S_AXI_GP0_WLAST;
   wire 				  BondOut_S_AXI_GP0_WREADY;
   wire [3:0] 				  BondOut_S_AXI_GP0_WSTRB;
   wire 				  BondOut_S_AXI_GP0_WVALID;

   
   
   // Processor subsystem instance
   HPR_ZYNQ_AXI4_GP0_MS_BONDOUT 
     #(C_M_AXI_GP0_THREAD_ID_WIDTH
       )
   the_ps7
     (

      .FCLK_CLK0(BondOut_FCLK_CLK0),
      .FCLK_RESET0_N(BondOut_FCLK_RESET0_N),      


        .FIXED_IO_mio(FIXED_IO_mio),


      .M_AXI_GP0_ARADDR(BondOut_M_AXI_GP0_ARADDR),
      .M_AXI_GP0_ARBURST(BondOut_M_AXI_GP0_ARBURST),
      .M_AXI_GP0_ARCACHE(BondOut_M_AXI_GP0_ARCACHE),
      .M_AXI_GP0_ARID(BondOut_M_AXI_GP0_ARID),
      .M_AXI_GP0_ARLEN(BondOut_M_AXI_GP0_ARLEN),
      .M_AXI_GP0_ARLOCK(BondOut_M_AXI_GP0_ARLOCK),
      .M_AXI_GP0_ARPROT(BondOut_M_AXI_GP0_ARPROT),
      .M_AXI_GP0_ARQOS(BondOut_M_AXI_GP0_ARQOS),
      .M_AXI_GP0_ARREADY(BondOut_M_AXI_GP0_ARREADY),
      .M_AXI_GP0_ARSIZE(BondOut_M_AXI_GP0_ARSIZE),
      .M_AXI_GP0_ARVALID(BondOut_M_AXI_GP0_ARVALID),

      .M_AXI_GP0_AWADDR(BondOut_M_AXI_GP0_AWADDR),
      .M_AXI_GP0_AWBURST(BondOut_M_AXI_GP0_AWBURST),
      .M_AXI_GP0_AWCACHE(BondOut_M_AXI_GP0_AWCACHE),
      .M_AXI_GP0_AWID(BondOut_M_AXI_GP0_AWID),
      .M_AXI_GP0_AWLEN(BondOut_M_AXI_GP0_AWLEN),
      .M_AXI_GP0_AWLOCK(BondOut_M_AXI_GP0_AWLOCK),
      .M_AXI_GP0_AWPROT(BondOut_M_AXI_GP0_AWPROT),
      .M_AXI_GP0_AWQOS(BondOut_M_AXI_GP0_AWQOS),
      .M_AXI_GP0_AWREADY(BondOut_M_AXI_GP0_AWREADY),
      .M_AXI_GP0_AWSIZE(BondOut_M_AXI_GP0_AWSIZE),
      .M_AXI_GP0_AWVALID(BondOut_M_AXI_GP0_AWVALID),

      .M_AXI_GP0_BID(BondOut_M_AXI_GP0_BID),
      .M_AXI_GP0_BREADY(BondOut_M_AXI_GP0_BREADY),
      .M_AXI_GP0_BRESP(BondOut_M_AXI_GP0_BRESP),
      .M_AXI_GP0_BVALID(BondOut_M_AXI_GP0_BVALID),

      .M_AXI_GP0_RDATA(BondOut_M_AXI_GP0_RDATA),
      .M_AXI_GP0_RID(BondOut_M_AXI_GP0_RID),
      .M_AXI_GP0_RLAST(BondOut_M_AXI_GP0_RLAST),
      .M_AXI_GP0_RREADY(BondOut_M_AXI_GP0_RREADY),
      .M_AXI_GP0_RRESP(BondOut_M_AXI_GP0_RRESP),
      .M_AXI_GP0_RVALID(BondOut_M_AXI_GP0_RVALID),

      .M_AXI_GP0_WDATA(BondOut_M_AXI_GP0_WDATA),
      .M_AXI_GP0_WID(BondOut_M_AXI_GP0_WID),
      .M_AXI_GP0_WLAST(BondOut_M_AXI_GP0_WLAST),
      .M_AXI_GP0_WREADY(BondOut_M_AXI_GP0_WREADY),
      .M_AXI_GP0_WSTRB(BondOut_M_AXI_GP0_WSTRB),
      .M_AXI_GP0_WVALID(BondOut_M_AXI_GP0_WVALID),


      .S_AXI_GP0_ARADDR(BondOut_S_AXI_GP0_ARADDR),
      .S_AXI_GP0_ARBURST(BondOut_S_AXI_GP0_ARBURST),
      .S_AXI_GP0_ARCACHE(BondOut_S_AXI_GP0_ARCACHE),
      .S_AXI_GP0_ARID(BondOut_S_AXI_GP0_ARID),
      .S_AXI_GP0_ARLEN(BondOut_S_AXI_GP0_ARLEN),
      .S_AXI_GP0_ARLOCK(BondOut_S_AXI_GP0_ARLOCK),
      .S_AXI_GP0_ARPROT(BondOut_S_AXI_GP0_ARPROT),
      .S_AXI_GP0_ARQOS(BondOut_S_AXI_GP0_ARQOS),
      .S_AXI_GP0_ARREADY(BondOut_S_AXI_GP0_ARREADY),
      .S_AXI_GP0_ARSIZE(BondOut_S_AXI_GP0_ARSIZE),
      .S_AXI_GP0_ARVALID(BondOut_S_AXI_GP0_ARVALID),

      .S_AXI_GP0_AWADDR(BondOut_S_AXI_GP0_AWADDR),
      .S_AXI_GP0_AWBURST(BondOut_S_AXI_GP0_AWBURST),
      .S_AXI_GP0_AWCACHE(BondOut_S_AXI_GP0_AWCACHE),
      .S_AXI_GP0_AWID(BondOut_S_AXI_GP0_AWID),
      .S_AXI_GP0_AWLEN(BondOut_S_AXI_GP0_AWLEN),
      .S_AXI_GP0_AWLOCK(BondOut_S_AXI_GP0_AWLOCK),
      .S_AXI_GP0_AWPROT(BondOut_S_AXI_GP0_AWPROT),
      .S_AXI_GP0_AWQOS(BondOut_S_AXI_GP0_AWQOS),
      .S_AXI_GP0_AWREADY(BondOut_S_AXI_GP0_AWREADY),
      .S_AXI_GP0_AWSIZE(BondOut_S_AXI_GP0_AWSIZE),
      .S_AXI_GP0_AWVALID(BondOut_S_AXI_GP0_AWVALID),

      .S_AXI_GP0_BID(BondOut_S_AXI_GP0_BID),
      .S_AXI_GP0_BREADY(BondOut_S_AXI_GP0_BREADY),
      .S_AXI_GP0_BRESP(BondOut_S_AXI_GP0_BRESP),
      .S_AXI_GP0_BVALID(BondOut_S_AXI_GP0_BVALID),

      .S_AXI_GP0_RDATA(BondOut_S_AXI_GP0_RDATA),
      .S_AXI_GP0_RID(BondOut_S_AXI_GP0_RID),
      .S_AXI_GP0_RLAST(BondOut_S_AXI_GP0_RLAST),
      .S_AXI_GP0_RREADY(BondOut_S_AXI_GP0_RREADY),
      .S_AXI_GP0_RRESP(BondOut_S_AXI_GP0_RRESP),
      .S_AXI_GP0_RVALID(BondOut_S_AXI_GP0_RVALID),

      .S_AXI_GP0_WDATA(BondOut_S_AXI_GP0_WDATA),
      .S_AXI_GP0_WID(BondOut_S_AXI_GP0_WID),
      .S_AXI_GP0_WLAST(BondOut_S_AXI_GP0_WLAST),
      .S_AXI_GP0_WREADY(BondOut_S_AXI_GP0_WREADY),
      .S_AXI_GP0_WSTRB(BondOut_S_AXI_GP0_WSTRB),
      .S_AXI_GP0_WVALID(BondOut_S_AXI_GP0_WVALID)

      );        



   
   HPR_AXI4_PIO_TARGET 
     #(C_M_AXI_GP0_THREAD_ID_WIDTH
//       DMA_DATA_WIDTH,
//       DMA_ADDR_WIDTH
       )

   
   kiwi_axi4_pio_target_i(
			  .clk(BondOut_FCLK_CLK0),
			  .reset_n(BondOut_FCLK_RESET0_N),

			  			  
			  // Kiwi Ksubs4 Standard Busses			  
			  .design_serial_number(design_serial_number),
			  .ksubs_runstop(ksubs_runstop),
			  .ksubsGpioSwitches(ksubsGpioSwitches),	
			  .ksubsGpioLeds(ksubsGpioLeds),
			  .misc_mon0(misc_mon0),
			  .misc_reg0(misc_reg0), 				
			  .result_hi(result_hi),  
			  .result_lo(result_lo),
			  .ksubsAbendSyndrome(ksubsAbendSyndrome),
			  .ksubsManualWaypoint(ksubsManualWaypoint),

			  // Programmed I/O access to director shim
			  .pio_hwen(pio_hwen), .pio_rdata(pio_rdata), .pio_wdata(pio_wdata), .pio_addr(pio_addr),


			      
			  // Ksubs Noc16 ring network - output of one component connects to input of next forming a big loop.
			  .Ksubs3_Noc16_TxData_lo(Ksubs3_Noc16_Arc0_Data_lo),
			  //.Ksubs3_Noc16_TxData_hi(Ksubs3_Noc16_Arc0_Data_hi),
			  .Ksubs3_Noc16_TxData_cmd(Ksubs3_Noc16_Arc0_Data_cmd),
			  .Ksubs3_Noc16_TxData_valid(Ksubs3_Noc16_Arc0_Data_valid),
			  .Ksubs3_Noc16_TxData_rdy(Ksubs3_Noc16_Arc0_Data_rdy),

			  
			  .Ksubs3_Noc16_RxData_lo(Ksubs3_Noc16_Arc1_Data_lo),
			  //.Ksubs3_Noc16_RxData_hi(Ksubs3_Noc16_Arc1_Data_hi),
			  .Ksubs3_Noc16_RxData_cmd(Ksubs3_Noc16_Arc1_Data_cmd),
			  .Ksubs3_Noc16_RxData_valid(Ksubs3_Noc16_Arc1_Data_valid),
			  .Ksubs3_Noc16_RxData_rdy(Ksubs3_Noc16_Arc1_Data_rdy),
			  
  

			  .maxi_arid(BondOut_M_AXI_GP0_ARID),			  
			  .maxi_araddr(BondOut_M_AXI_GP0_ARADDR),
			  .maxi_arprot(BondOut_M_AXI_GP0_ARPROT),
			  .maxi_arready(BondOut_M_AXI_GP0_ARREADY),
			  .maxi_arvalid(BondOut_M_AXI_GP0_ARVALID),
			  
			  .maxi_awid(BondOut_M_AXI_GP0_AWID),
			  .maxi_awaddr(BondOut_M_AXI_GP0_AWADDR),
			  .maxi_awprot(BondOut_M_AXI_GP0_AWPROT),
			  .maxi_awready(BondOut_M_AXI_GP0_AWREADY),
			  .maxi_awvalid(BondOut_M_AXI_GP0_AWVALID),
			  
			  .maxi_bid(BondOut_M_AXI_GP0_BID),
			  .maxi_bready(BondOut_M_AXI_GP0_BREADY),
			  .maxi_bresp(BondOut_M_AXI_GP0_BRESP),
			  .maxi_bvalid(BondOut_M_AXI_GP0_BVALID),
			  
			  .maxi_rid(BondOut_M_AXI_GP0_RID),
			  .maxi_rdata(BondOut_M_AXI_GP0_RDATA),
			  .maxi_rready(BondOut_M_AXI_GP0_RREADY),
			  .maxi_rresp(BondOut_M_AXI_GP0_RRESP),
			  .maxi_rlast(BondOut_M_AXI_GP0_RLAST),
			  .maxi_rvalid(BondOut_M_AXI_GP0_RVALID),
			  
			  .maxi_wid(BondOut_M_AXI_GP0_WID),
			  .maxi_wdata(BondOut_M_AXI_GP0_WDATA),
			  .maxi_wready(BondOut_M_AXI_GP0_WREADY),
			  .maxi_wstrb(BondOut_M_AXI_GP0_WSTRB),
			  .maxi_wvalid(BondOut_M_AXI_GP0_WVALID));


   HPR_HFAST_TO_AXI4_MASTER // This component enables the role design access to the blade DRAM.
     #(C_S_AXI_GP0_THREAD_ID_WIDTH
       )
   hpr_hfast_to_axi4_master_i(
			      .clk(BondOut_FCLK_CLK0),
			      .reset_n(BondOut_FCLK_RESET0_N),
			      
			      .axi_status(mon0),
			      .hfast_addr(hfast_addr),
			      .hfast_wdata(hfast_wdata),
			      .hfast_rdata(hfast_rdata),
			      .hfast_req(hfast_req),
			      .hfast_rwbar(hfast_rwbar),
			      .hfast_ack(hfast_ack),			

			      
			      
			      .saxi_ARID(BondOut_S_AXI_GP0_ARID),			  
			      .saxi_ARADDR(BondOut_S_AXI_GP0_ARADDR),
			      .saxi_ARPROT(BondOut_S_AXI_GP0_ARPROT),
			      .saxi_ARREADY(BondOut_S_AXI_GP0_ARREADY),
			      .saxi_ARVALID(BondOut_S_AXI_GP0_ARVALID),
			      
			      .saxi_AWID(BondOut_S_AXI_GP0_AWID),
			      .saxi_AWADDR(BondOut_S_AXI_GP0_AWADDR),
			      .saxi_AWPROT(BondOut_S_AXI_GP0_AWPROT),
			      .saxi_AWREADY(BondOut_S_AXI_GP0_AWREADY),
			      .saxi_AWVALID(BondOut_S_AXI_GP0_AWVALID),
			      
			      .saxi_BID(BondOut_S_AXI_GP0_BID),
			      .saxi_BREADY(BondOut_S_AXI_GP0_BREADY),
			      .saxi_BRESP(BondOut_S_AXI_GP0_BRESP),
			      .saxi_BVALID(BondOut_S_AXI_GP0_BVALID),
			      
			      .saxi_RID(BondOut_S_AXI_GP0_RID),
			      .saxi_RDATA(BondOut_S_AXI_GP0_RDATA),
			      .saxi_RREADY(BondOut_S_AXI_GP0_RREADY),
			      .saxi_RRESP(BondOut_S_AXI_GP0_RRESP),
			      .saxi_RLAST(BondOut_S_AXI_GP0_RLAST),
			      .saxi_RVALID(BondOut_S_AXI_GP0_RVALID),
			      
			      .saxi_WID(BondOut_S_AXI_GP0_WID),
			      .saxi_WDATA(BondOut_S_AXI_GP0_WDATA),
			      .saxi_WREADY(BondOut_S_AXI_GP0_WREADY),
			      .saxi_WSTRB(BondOut_S_AXI_GP0_WSTRB),
			      .saxi_WVALID(BondOut_S_AXI_GP0_WVALID));


endmodule // toplevel
// eof



