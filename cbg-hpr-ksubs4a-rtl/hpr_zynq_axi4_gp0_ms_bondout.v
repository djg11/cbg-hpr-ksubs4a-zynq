//
module HPR_ZYNQ_AXI4_GP0_MS_BONDOUT
#( 
   parameter integer C_M_AXI_GP0_THREAD_ID_WIDTH = 12,
   parameter integer C_S_AXI_GP0_THREAD_ID_WIDTH = 12
)
(

 output 				  FCLK_CLK0,
 output 				  FCLK_RESET0_N, 

 // M_AXI_GP0 - PIO from ARM
 output 				  M_AXI_GP0_ARVALID,
 output 				  M_AXI_GP0_AWVALID,
 output 				  M_AXI_GP0_BREADY,
 output 				  M_AXI_GP0_RREADY,
 output 				  M_AXI_GP0_WLAST,
 output 				  M_AXI_GP0_WVALID,
 output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] M_AXI_GP0_ARID,
 output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] M_AXI_GP0_AWID,
 output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] M_AXI_GP0_WID,
 output [1:0] 				  M_AXI_GP0_ARBURST,
 output [1:0] 				  M_AXI_GP0_ARLOCK,
 output [2:0] 				  M_AXI_GP0_ARSIZE,
 output [1:0] 				  M_AXI_GP0_AWBURST,
 output [1:0] 				  M_AXI_GP0_AWLOCK,
 output [2:0] 				  M_AXI_GP0_AWSIZE,
 output [2:0] 				  M_AXI_GP0_ARPROT,
 output [2:0] 				  M_AXI_GP0_AWPROT,
 output [31:0] 				  M_AXI_GP0_ARADDR,
 output [31:0] 				  M_AXI_GP0_AWADDR,
 output [31:0] 				  M_AXI_GP0_WDATA,
 output [3:0] 				  M_AXI_GP0_ARCACHE,
 output [3:0] 				  M_AXI_GP0_ARLEN,
 output [3:0] 				  M_AXI_GP0_ARQOS,
 output [3:0] 				  M_AXI_GP0_AWCACHE,
 output [3:0] 				  M_AXI_GP0_AWLEN,
 output [3:0] 				  M_AXI_GP0_AWQOS,
 output [3:0] 				  M_AXI_GP0_WSTRB, 
 //
 input 					  M_AXI_GP0_ARREADY,
 input 					  M_AXI_GP0_AWREADY,
 input 					  M_AXI_GP0_BVALID,
 input 					  M_AXI_GP0_RLAST,
 input 					  M_AXI_GP0_RVALID,
 input 					  M_AXI_GP0_WREADY,
 input [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0]  M_AXI_GP0_BID,
 input [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0]  M_AXI_GP0_RID,
 input [1:0] 				  M_AXI_GP0_BRESP,
 input [1:0] 				  M_AXI_GP0_RRESP,
 input [31:0] 				  M_AXI_GP0_RDATA,



 // S_AXI_GP0 - Data to DRAM
 input 					  S_AXI_GP0_ARVALID,
 input 					  S_AXI_GP0_AWVALID,
 input 					  S_AXI_GP0_BREADY,
 input 					  S_AXI_GP0_RREADY,
 input 					  S_AXI_GP0_WLAST,
 input 					  S_AXI_GP0_WVALID,
 input [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0]  S_AXI_GP0_ARID,
 input [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0]  S_AXI_GP0_AWID,
 input [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0]  S_AXI_GP0_WID,
 input [1:0] 				  S_AXI_GP0_ARBURST,
 input [1:0] 				  S_AXI_GP0_ARLOCK,
 input [2:0] 				  S_AXI_GP0_ARSIZE,
 input [1:0] 				  S_AXI_GP0_AWBURST,
 input [1:0] 				  S_AXI_GP0_AWLOCK,
 input [2:0] 				  S_AXI_GP0_AWSIZE,
 input [2:0] 				  S_AXI_GP0_ARPROT,
 input [2:0] 				  S_AXI_GP0_AWPROT,
 input [31:0] 				  S_AXI_GP0_ARADDR,
 input [31:0] 				  S_AXI_GP0_AWADDR,
 input [31:0] 				  S_AXI_GP0_WDATA,
 input [3:0] 				  S_AXI_GP0_ARCACHE,
 input [3:0] 				  S_AXI_GP0_ARLEN,
 input [3:0] 				  S_AXI_GP0_ARQOS,
 input [3:0] 				  S_AXI_GP0_AWCACHE,
 input [3:0] 				  S_AXI_GP0_AWLEN,
 input [3:0] 				  S_AXI_GP0_AWQOS,
 input [3:0] 				  S_AXI_GP0_WSTRB, 
 //
 output 				  S_AXI_GP0_ARREADY,
 output 				  S_AXI_GP0_AWREADY,
 output 				  S_AXI_GP0_BVALID,
 output 				  S_AXI_GP0_RLAST,
 output 				  S_AXI_GP0_RVALID,
 output 				  S_AXI_GP0_WREADY,
 output [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0] S_AXI_GP0_BID,
 output [C_S_AXI_GP0_THREAD_ID_WIDTH-1:0] S_AXI_GP0_RID,
 output [1:0] 				  S_AXI_GP0_BRESP,
 output [1:0] 				  S_AXI_GP0_RRESP,
 output [31:0] 				  S_AXI_GP0_RDATA,

 
			       
 inout [53:0] 				  FIXED_IO_mio	
 );

   wire GND_1;
     GND GND
       (.G(GND_1));

   processing_system7_v5_5_processing_system7 the_proc7
       (
        .MIO(FIXED_IO_mio[53:0]),

	// Clock and Reset Generators
	.FCLK_CLK0(FCLK_CLK0),
	.FCLK_RESET0_N(FCLK_RESET0_N),      



	// Three more inouts
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),


	// M_AXI_GP0
        .M_AXI_GP0_ACLK(FCLK_CLK0),
        .M_AXI_GP0_ARADDR(M_AXI_GP0_ARADDR),
        .M_AXI_GP0_ARBURST(M_AXI_GP0_ARBURST),
        .M_AXI_GP0_ARCACHE(M_AXI_GP0_ARCACHE),
        .M_AXI_GP0_ARID(M_AXI_GP0_ARID),
        .M_AXI_GP0_ARLEN(M_AXI_GP0_ARLEN),
        .M_AXI_GP0_ARLOCK(M_AXI_GP0_ARLOCK),
        .M_AXI_GP0_ARPROT(M_AXI_GP0_ARPROT),
        .M_AXI_GP0_ARQOS(M_AXI_GP0_ARQOS),
        .M_AXI_GP0_ARREADY(M_AXI_GP0_ARREADY),
        .M_AXI_GP0_ARSIZE(M_AXI_GP0_ARSIZE),
        .M_AXI_GP0_ARVALID(M_AXI_GP0_ARVALID),
        .M_AXI_GP0_AWADDR(M_AXI_GP0_AWADDR),
        .M_AXI_GP0_AWBURST(M_AXI_GP0_AWBURST),
        .M_AXI_GP0_AWCACHE(M_AXI_GP0_AWCACHE),
        .M_AXI_GP0_AWID(M_AXI_GP0_AWID),
        .M_AXI_GP0_AWLEN(M_AXI_GP0_AWLEN),
        .M_AXI_GP0_AWLOCK(M_AXI_GP0_AWLOCK),
        .M_AXI_GP0_AWPROT(M_AXI_GP0_AWPROT),
        .M_AXI_GP0_AWQOS(M_AXI_GP0_AWQOS),
        .M_AXI_GP0_AWREADY(M_AXI_GP0_AWREADY),
        .M_AXI_GP0_AWSIZE(M_AXI_GP0_AWSIZE),
        .M_AXI_GP0_AWVALID(M_AXI_GP0_AWVALID),
        .M_AXI_GP0_BID(M_AXI_GP0_BID),
        .M_AXI_GP0_BREADY(M_AXI_GP0_BREADY),
        .M_AXI_GP0_BRESP(M_AXI_GP0_BRESP),
        .M_AXI_GP0_BVALID(M_AXI_GP0_BVALID),
        .M_AXI_GP0_RDATA(M_AXI_GP0_RDATA),
        .M_AXI_GP0_RID(M_AXI_GP0_RID),
        .M_AXI_GP0_RLAST(M_AXI_GP0_RLAST),
        .M_AXI_GP0_RREADY(M_AXI_GP0_RREADY),
        .M_AXI_GP0_RRESP(M_AXI_GP0_RRESP),
        .M_AXI_GP0_RVALID(M_AXI_GP0_RVALID),
        .M_AXI_GP0_WDATA(M_AXI_GP0_WDATA),
        .M_AXI_GP0_WID(M_AXI_GP0_WID),
        .M_AXI_GP0_WLAST(M_AXI_GP0_WLAST),
        .M_AXI_GP0_WREADY(M_AXI_GP0_WREADY),
        .M_AXI_GP0_WSTRB(M_AXI_GP0_WSTRB),
        .M_AXI_GP0_WVALID(M_AXI_GP0_WVALID),

	// S_AXI_GP0
        .S_AXI_GP0_ACLK(FCLK_CLK0),
        .S_AXI_GP0_ARADDR(S_AXI_GP0_ARADDR),
        .S_AXI_GP0_ARBURST(S_AXI_GP0_ARBURST),
        .S_AXI_GP0_ARCACHE(S_AXI_GP0_ARCACHE),
        .S_AXI_GP0_ARID(S_AXI_GP0_ARID),
        .S_AXI_GP0_ARLEN(S_AXI_GP0_ARLEN),
        .S_AXI_GP0_ARLOCK(S_AXI_GP0_ARLOCK),
        .S_AXI_GP0_ARPROT(S_AXI_GP0_ARPROT),
        .S_AXI_GP0_ARQOS(S_AXI_GP0_ARQOS),
        .S_AXI_GP0_ARREADY(S_AXI_GP0_ARREADY),
        .S_AXI_GP0_ARSIZE(S_AXI_GP0_ARSIZE),
        .S_AXI_GP0_ARVALID(S_AXI_GP0_ARVALID),
        .S_AXI_GP0_AWADDR(S_AXI_GP0_AWADDR),
        .S_AXI_GP0_AWBURST(S_AXI_GP0_AWBURST),
        .S_AXI_GP0_AWCACHE(S_AXI_GP0_AWCACHE),
        .S_AXI_GP0_AWID(S_AXI_GP0_AWID),
        .S_AXI_GP0_AWLEN(S_AXI_GP0_AWLEN),
        .S_AXI_GP0_AWLOCK(S_AXI_GP0_AWLOCK),
        .S_AXI_GP0_AWPROT(S_AXI_GP0_AWPROT),
        .S_AXI_GP0_AWQOS(S_AXI_GP0_AWQOS),
        .S_AXI_GP0_AWREADY(S_AXI_GP0_AWREADY),
        .S_AXI_GP0_AWSIZE(S_AXI_GP0_AWSIZE),
        .S_AXI_GP0_AWVALID(S_AXI_GP0_AWVALID),
        .S_AXI_GP0_BID(S_AXI_GP0_BID),
        .S_AXI_GP0_BREADY(S_AXI_GP0_BREADY),
        .S_AXI_GP0_BRESP(S_AXI_GP0_BRESP),
        .S_AXI_GP0_BVALID(S_AXI_GP0_BVALID),
        .S_AXI_GP0_RDATA(S_AXI_GP0_RDATA),
        .S_AXI_GP0_RID(S_AXI_GP0_RID),
        .S_AXI_GP0_RLAST(S_AXI_GP0_RLAST),
        .S_AXI_GP0_RREADY(S_AXI_GP0_RREADY),
        .S_AXI_GP0_RRESP(S_AXI_GP0_RRESP),
        .S_AXI_GP0_RVALID(S_AXI_GP0_RVALID),
        .S_AXI_GP0_WDATA(S_AXI_GP0_WDATA),
        .S_AXI_GP0_WID(S_AXI_GP0_WID),
        .S_AXI_GP0_WLAST(S_AXI_GP0_WLAST),
        .S_AXI_GP0_WREADY(S_AXI_GP0_WREADY),
        .S_AXI_GP0_WSTRB(S_AXI_GP0_WSTRB),
        .S_AXI_GP0_WVALID(S_AXI_GP0_WVALID)

	);

endmodule
// eof
