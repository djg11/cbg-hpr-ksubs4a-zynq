// Kiwi Scientific Acceleration
// (C) 2014 DJ Greaves - University of Cambridge, Computer Laboratory
//
//
// hpr_hfast_to_axi4_master.v
// Note, Zynq reqyireds 0xF8000900 LVL_SHFTR_EN to have all four bottom bits set of bidir comms.




//  The slave asserts READY when it can accept new transfers (VALID doesn't have to be asserted for this).
//  The master asserts VALID when it has something to transfer (READY doesn't have to be asserted for this).
//  A master can't wait with asserting VALID until a READY is asserted since the slave can wait for VALID before asserting READY.
//  If the master waits for READY, this can create a deadlock where both master and slaves waits for each other to assert the signal.

	 
// Note: to avoid deadlock the VALID signal of one AXI component must not be dependent on the READY signal of the other component 
// in the transaction and the READY signal can wait for assertion of the VALID signal.

//
// AXI4 Target.
//
module HPR_HFAST_TO_AXI4_MASTER
#(  parameter integer C_M_AXI_GP0_THREAD_ID_WIDTH = 12,
    parameter integer DMA_DATA_WIDTH = 32,
    parameter integer DMA_ADDR_WIDTH = 32
)
(
  input 				   clk,
  input 				   reset_n,


 // HFAST1 PORT SLAVE
  input [DMA_ADDR_WIDTH-1:0] 		   hfast_addr,
  input [DMA_DATA_WIDTH-1:0] 		   hfast_wdata,
  output [DMA_DATA_WIDTH-1:0] 		   hfast_rdata, 
  input 				   hfast_req,
  input 				   hfast_rwbar, 
  output reg 				   hfast_ack, 

  output [31:0] 			   axi_status,
 
  // AXI4 BUS MASTER
  output reg [31:0] 			   saxi_ARADDR, // Read address
  output reg [2:0] 			   saxi_ARPROT,
  output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] saxi_ARID,
  output reg 				   saxi_ARVALID,
  input 				   saxi_ARREADY,

  input [31:0] 				   saxi_RDATA, // Read data
  input [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0]  saxi_RID,
  input [1:0] 				   saxi_RRESP,
  input 				   saxi_RLAST,
  input 				   saxi_RVALID,
  output 				   saxi_RREADY,
 
  output reg [31:0] 			   saxi_AWADDR, // Write address
  output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] saxi_AWID,
  output reg [2:0] 			   saxi_AWPROT,
  output reg 				   saxi_AWVALID,
  input 				   saxi_AWREADY,

  output reg [31:0] 			   saxi_WDATA, // Write data
  output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] saxi_WID,
  output reg [3:0] 			   saxi_WSTRB,
  output reg 				   saxi_WVALID,
  input 				   saxi_WREADY,
 
  input [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0]  saxi_BID,
  input [1:0] 				   saxi_BRESP,
  input 				   saxi_BVALID,
  output 				   saxi_BREADY // Write response
 
 );
  
//  assign maxi_arready = 1; // always ready
//  assign maxi_awready = 1; // always ready
//  assign maxi_wready = 1;  // always ready

//   reg [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] rid, wid;
   assign saxi_ARID = 0;
   assign saxi_RID = 0;
   assign saxi_WID = 0;
        


   reg [DMA_DATA_WIDTH-1:0] 	    wd_hold;
   reg [DMA_ADDR_WIDTH-1:0] 	    a_hold;
   reg [DMA_DATA_WIDTH-1:0] 	    rd_hold;
   reg 				    wd_sent, aw_sent, ar_sent, rd_sent;
   reg 				    rd_pending, wr_pending;
   integer 			    traceme = 0;

   reg [7:0] 			    rd_ops, rd_replies;
   
   always @(posedge clk) if (!reset_n) begin
      saxi_ARADDR  <= 0;
      saxi_ARVALID <= 0;
      saxi_ARPROT <= 0;
      saxi_WVALID <= 0;
      saxi_AWVALID <= 0;      

      rd_ops <= 0;
      rd_replies <=0;

      ar_sent <= 0;
      rd_hold <= 32'h5512;
      hfast_ack <= 0;
      rd_pending <= 0;
      wr_pending <= 0;
      aw_sent <= 0;		
      wd_sent <= 0;		
   end
   else begin
      if (hfast_req && hfast_rwbar) begin // Read start
	 a_hold  <= hfast_addr;
	 rd_pending <= 1;
      end
      else if (hfast_req && !hfast_rwbar) begin // Write start	 - posting with early ack
	 wd_hold <= hfast_wdata;
	 a_hold  <= hfast_addr;
	wr_pending <= 1;
      end
      
      if (wr_pending && aw_sent && wd_sent) begin
	 aw_sent <= 0;
	 wd_sent <=0;
	 wr_pending <=0;
	 hfast_ack <= 1;
      end
      else
	begin
	   if (wr_pending && !aw_sent) begin
	      saxi_AWVALID <= 1;
	      saxi_AWADDR <= a_hold;
	      saxi_AWPROT  <= 0;    
	   end
	   
	   if (wr_pending && !wd_sent) begin
	      saxi_WVALID <=  1;
	      saxi_WSTRB  <=  4'd15; // All lanes
	      saxi_WDATA  <=  wd_hold;
	   end
	end //

      
      if (saxi_AWVALID && saxi_AWREADY) begin
	 saxi_AWVALID <= 0;
	 aw_sent <= 1;
      end
      
      if (saxi_WVALID && saxi_WREADY) begin	
	 wd_sent <= 1;
	 saxi_WVALID <= 0;
      end
     
    
     if (saxi_RVALID && saxi_RREADY) begin // Read response comes back - ignores response code!
        ar_sent <=     0;
	rd_hold <=     saxi_RDATA;
	rd_pending <=  0;
	hfast_ack <=   1;
	rd_replies <= rd_replies + 1;
	end

     else if (saxi_ARVALID && saxi_ARREADY) begin // Read address phase complete
	saxi_ARVALID <= 0;
	ar_sent <= 1;
	rd_ops <= rd_ops + 1;
     end
     
     else if (rd_pending  && !ar_sent) begin // Send Read address
	saxi_ARADDR <= a_hold;
	saxi_ARVALID <= 1;
	saxi_ARPROT <= 0;
     end

     if (hfast_ack) hfast_ack <= 0;
   end

   
   assign hfast_rdata = (saxi_RVALID && saxi_RREADY) ? saxi_RDATA:rd_hold;

   assign saxi_BREADY = 1; // always true
   assign saxi_RREADY = rd_pending;

   assign axi_status = { 1'b1, rd_replies, rd_ops };
endmodule
// EOF

