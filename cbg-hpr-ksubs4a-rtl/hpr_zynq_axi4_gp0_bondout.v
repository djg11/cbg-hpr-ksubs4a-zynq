//
module HPR_ZYNQ_AXI4_GP0_BONDOUT

#(  parameter integer C_M_AXI_GP0_THREAD_ID_WIDTH = 12
)
(

 output 				      FCLK_CLK0,
 output 				      FCLK_RESET0_N, 

 // M_AXI_GP0
 output 				      M_AXI_GP0_ARVALID,
 output 				      M_AXI_GP0_AWVALID,
 output 				      M_AXI_GP0_BREADY,
 output 				      M_AXI_GP0_RREADY,
 output 				      M_AXI_GP0_WLAST,
 output 				      M_AXI_GP0_WVALID,
 output [(C_M_AXI_GP0_THREAD_ID_WIDTH - 1):0] M_AXI_GP0_ARID,
 output [(C_M_AXI_GP0_THREAD_ID_WIDTH - 1):0] M_AXI_GP0_AWID,
 output [(C_M_AXI_GP0_THREAD_ID_WIDTH - 1):0] M_AXI_GP0_WID,
 output [1:0] 				      M_AXI_GP0_ARBURST,
 output [1:0] 				      M_AXI_GP0_ARLOCK,
 output [2:0] 				      M_AXI_GP0_ARSIZE,
 output [1:0] 				      M_AXI_GP0_AWBURST,
 output [1:0] 				      M_AXI_GP0_AWLOCK,
 output [2:0] 				      M_AXI_GP0_AWSIZE,
 output [2:0] 				      M_AXI_GP0_ARPROT,
 output [2:0] 				      M_AXI_GP0_AWPROT,
 output [31:0] 				      M_AXI_GP0_ARADDR,
 output [31:0] 				      M_AXI_GP0_AWADDR,
 output [31:0] 				      M_AXI_GP0_WDATA,
 output [3:0] 				      M_AXI_GP0_ARCACHE,
 output [3:0] 				      M_AXI_GP0_ARLEN,
 output [3:0] 				      M_AXI_GP0_ARQOS,
 output [3:0] 				      M_AXI_GP0_AWCACHE,
 output [3:0] 				      M_AXI_GP0_AWLEN,
 output [3:0] 				      M_AXI_GP0_AWQOS,
 output [3:0] 				      M_AXI_GP0_WSTRB, 
 //
 input 					      M_AXI_GP0_ARREADY,
 input 					      M_AXI_GP0_AWREADY,
 input 					      M_AXI_GP0_BVALID,
 input 					      M_AXI_GP0_RLAST,
 input 					      M_AXI_GP0_RVALID,
 input 					      M_AXI_GP0_WREADY,
 input [(C_M_AXI_GP0_THREAD_ID_WIDTH - 1):0]  M_AXI_GP0_BID,
 input [(C_M_AXI_GP0_THREAD_ID_WIDTH - 1):0]  M_AXI_GP0_RID,
 input [1:0] 				      M_AXI_GP0_BRESP,
 input [1:0] 				      M_AXI_GP0_RRESP,
 input [31:0] 				      M_AXI_GP0_RDATA,
 
			       
 inout [53:0] 				      FIXED_IO_mio	
		       );

   wire GND_1;
     GND GND
       (.G(GND_1));

   processing_system7_v5_5_processing_system7 the_proc7
       (
`ifdef DRAMOLD	
	.DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
`endif
        .MIO(FIXED_IO_mio[53:0]),

	// Clock and Reset Generators
	.FCLK_CLK0(FCLK_CLK0),
	.FCLK_RESET0_N(FCLK_RESET0_N),      



	// Three more inouts
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),


	// M_AXI_GP0
        .M_AXI_GP0_ACLK(FCLK_CLK0),
        .M_AXI_GP0_ARADDR(M_AXI_GP0_ARADDR),
        .M_AXI_GP0_ARBURST(M_AXI_GP0_ARBURST),
        .M_AXI_GP0_ARCACHE(M_AXI_GP0_ARCACHE),
        .M_AXI_GP0_ARID(M_AXI_GP0_ARID),
        .M_AXI_GP0_ARLEN(M_AXI_GP0_ARLEN),
        .M_AXI_GP0_ARLOCK(M_AXI_GP0_ARLOCK),
        .M_AXI_GP0_ARPROT(M_AXI_GP0_ARPROT),
        .M_AXI_GP0_ARQOS(M_AXI_GP0_ARQOS),
        .M_AXI_GP0_ARREADY(M_AXI_GP0_ARREADY),
        .M_AXI_GP0_ARSIZE(M_AXI_GP0_ARSIZE),
        .M_AXI_GP0_ARVALID(M_AXI_GP0_ARVALID),
        .M_AXI_GP0_AWADDR(M_AXI_GP0_AWADDR),
        .M_AXI_GP0_AWBURST(M_AXI_GP0_AWBURST),
        .M_AXI_GP0_AWCACHE(M_AXI_GP0_AWCACHE),
        .M_AXI_GP0_AWID(M_AXI_GP0_AWID),
        .M_AXI_GP0_AWLEN(M_AXI_GP0_AWLEN),
        .M_AXI_GP0_AWLOCK(M_AXI_GP0_AWLOCK),
        .M_AXI_GP0_AWPROT(M_AXI_GP0_AWPROT),
        .M_AXI_GP0_AWQOS(M_AXI_GP0_AWQOS),
        .M_AXI_GP0_AWREADY(M_AXI_GP0_AWREADY),
        .M_AXI_GP0_AWSIZE(M_AXI_GP0_AWSIZE),
        .M_AXI_GP0_AWVALID(M_AXI_GP0_AWVALID),
        .M_AXI_GP0_BID(M_AXI_GP0_BID),
        .M_AXI_GP0_BREADY(M_AXI_GP0_BREADY),
        .M_AXI_GP0_BRESP(M_AXI_GP0_BRESP),
        .M_AXI_GP0_BVALID(M_AXI_GP0_BVALID),
        .M_AXI_GP0_RDATA(M_AXI_GP0_RDATA),
        .M_AXI_GP0_RID(M_AXI_GP0_RID),
        .M_AXI_GP0_RLAST(M_AXI_GP0_RLAST),
        .M_AXI_GP0_RREADY(M_AXI_GP0_RREADY),
        .M_AXI_GP0_RRESP(M_AXI_GP0_RRESP),
        .M_AXI_GP0_RVALID(M_AXI_GP0_RVALID),
        .M_AXI_GP0_WDATA(M_AXI_GP0_WDATA),
        .M_AXI_GP0_WID(M_AXI_GP0_WID),
        .M_AXI_GP0_WLAST(M_AXI_GP0_WLAST),
        .M_AXI_GP0_WREADY(M_AXI_GP0_WREADY),
        .M_AXI_GP0_WSTRB(M_AXI_GP0_WSTRB),
        .M_AXI_GP0_WVALID(M_AXI_GP0_WVALID),


	// S_AXI_GP0
        .S_AXI_GP0_ACLK(FCLK_CLK0),
        .S_AXI_GP0_ARADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_ARBURST({GND_1,GND_1}),
        .S_AXI_GP0_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_ARID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_ARLEN({GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_ARLOCK({GND_1,GND_1}),
        .S_AXI_GP0_ARPROT({GND_1,GND_1,GND_1}),
        .S_AXI_GP0_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_ARSIZE({GND_1,GND_1,GND_1}),
        .S_AXI_GP0_ARVALID(GND_1),
        .S_AXI_GP0_AWADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_AWBURST({GND_1,GND_1}),
        .S_AXI_GP0_AWCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_AWID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_AWLEN({GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_AWLOCK({GND_1,GND_1}),
        .S_AXI_GP0_AWPROT({GND_1,GND_1,GND_1}),
        .S_AXI_GP0_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_AWSIZE({GND_1,GND_1,GND_1}),
        .S_AXI_GP0_AWVALID(GND_1),
        .S_AXI_GP0_BREADY(GND_1),
        .S_AXI_GP0_RREADY(GND_1),
        .S_AXI_GP0_WDATA({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_WID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_WLAST(GND_1),
        .S_AXI_GP0_WSTRB({GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_GP0_WVALID(GND_1));
endmodule
