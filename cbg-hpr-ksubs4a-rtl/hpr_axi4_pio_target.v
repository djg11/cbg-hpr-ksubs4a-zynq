// Kiwi Scientific Acceleration
// (C) 2014 DJ Greaves - University of Cambridge, Computer Laboratory
//
//
//
// kiwi_axi4_pio_target.v
//




//  The slave asserts READY when it can accept new transfers (VALID doesn't have to be asserted for this).
//  The master asserts VALID when it has something to transfer (READY doesn't have to be asserted for this).
//  A master can't wait with asserting VALID until a READY is asserted since the slave can wait for VALID before asserting READY.
//  If the master waits for READY, this can create a deadlock where both master and slaves waits for each other to assert the signal.

	 
// Note: to avoid deadlock the VALID signal of one AXI component must not be dependent on the READY signal of the other component 
// in the transaction and the READY signal can wait for assertion of the VALID signal.

//
// AXI4 Target.
//
module HPR_AXI4_PIO_TARGET

#(  parameter integer C_M_AXI_GP0_THREAD_ID_WIDTH = 12
)

(
  input 				   clk,
  input 				   reset_n,

 // Kiwi Ksubs4 Standard Busses
  input [23:0] 				   design_serial_number, 
  output [1:0] 				   ksubs_runstop, // Single-step and reset control
  input [7:0] 				   ksubsGpioSwitches, // Physical switches on the FPGA blade
  input [7:0] 				   ksubsGpioLeds, // Leds on the blade
  input [31:0] 				   misc_mon0, // General purpose application-specific 32-bit Role output for PIO reading
  output reg [31:0] 			   misc_reg0, // General purpose application-specific 32-bit bus - PIO output to Role
  input [31:0] 				   ksubsManualWaypoint, // Waypoint indication
  input [31:0] 				   result_lo, result_hi, // Further role output for PIO reading (like misc_mon) but normally used for simple computation result.
  input [7:0] 				   ksubsAbendSyndrome, // Kiwi Run/Stop indication and runtime error or exit code reporting.



 // Programmed I/O access to director shim
  output [11:0] 			   pio_addr, // Only 10 bits can be exercised of this address bus with the simple devmem2 skeleton!
  input [31:0] 				   pio_rdata,
  output [31:0] 			   pio_wdata,
  output 				   pio_hwen,


 // Slotted ring - slots have 72 bits in them. Any station can write to a slot whose top
 // byte is zero, which denotes not in use.  Stations should pass on all other slots
 // unchanged unless they are responding to a command addressed to themselves.
 
 // Ksubs Noc16 duplex interface: server-side net directions.
  input [63:0] 				   Ksubs3_Noc16_TxData_lo,
 //input [63:0] 	Ksubs3_Noc16_TxData_hi,
  input [7:0] 				   Ksubs3_Noc16_TxData_cmd,
  input 				   Ksubs3_Noc16_TxData_valid,
  output 				   Ksubs3_Noc16_TxData_rdy,
 
  output [63:0] 			   Ksubs3_Noc16_RxData_lo,
 //output [63:0] Ksubs3_Noc16_RxData_hi,
  output [7:0] 				   Ksubs3_Noc16_RxData_cmd,
  output 				   Ksubs3_Noc16_RxData_valid,
  input 				   Ksubs3_Noc16_RxData_rdy,
			     

 

  // AXI4 BUS SLAVE
  input [31:0] 				   maxi_araddr, // Read address
  input [2:0] 				   maxi_arprot,
  input [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0]  maxi_arid,
  output 				   maxi_arready,
  input 				   maxi_arvalid,


  output [31:0] 			   maxi_rdata, // Read data
  output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] maxi_rid,
  input 				   maxi_rready,
  output [1:0] 				   maxi_rresp,
  output 				   maxi_rlast,
  output reg 				   maxi_rvalid,

  input [31:0] 				   maxi_awaddr, // Write address
  input [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0]  maxi_awid,
  input [2:0] 				   maxi_awprot,
  output 				   maxi_awready,
  input 				   maxi_awvalid,


  input [31:0] 				   maxi_wdata, // Write data
  input [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0]  maxi_wid,
  output 				   maxi_wready,
  input [3:0] 				   maxi_wstrb,
  input 				   maxi_wvalid,

  input 				   maxi_bready, // Write response
  output [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] maxi_bid,
  output reg [1:0] 			   maxi_bresp,
  output reg 				   maxi_bvalid
 );
  
  assign maxi_arready = 1; // always ready
  assign maxi_awready = 1; // always ready
  assign maxi_wready = 1;  // always ready



   reg 	    have_wdata, have_waddr;
   wire	    wstrobe = have_wdata && have_waddr;

   reg [C_M_AXI_GP0_THREAD_ID_WIDTH-1:0] rid, wid;
   
  always @(posedge clk) if (!reset_n) begin
     maxi_rvalid <= 0;
     maxi_bvalid <= 0; have_wdata <= 0; have_waddr <= 0;
     maxi_bresp <= 2'd0; // code 00 is ok 
     end
     else begin
	// RVALID
        if (maxi_arready && maxi_arvalid) begin
	   rid <= maxi_arid;
	   maxi_rvalid <= 1;
	   end
        else if (maxi_rvalid && maxi_rready) maxi_rvalid <=0;

        if (maxi_awready && maxi_awvalid && maxi_wready && maxi_wvalid) begin // Fast write
	   if (maxi_awid != maxi_wid) begin
	      maxi_bresp = 2'b10; // SLAVE ERROR CODE.
	      end
	   wid <= maxi_awid;
	   have_waddr <= 1;
	   have_wdata <= 1;	   
	end
	   
        else if (maxi_awready && maxi_awvalid) begin // Staggered write - addr first
	   wid <= maxi_awid;
	   have_waddr <= 1;
	   end

        if (maxi_wready && maxi_wvalid) begin // Staggered write - data first
	   wid <= maxi_wid;
	   have_wdata <= 1;	
	   end

	// BVALID
        if (!maxi_bvalid && have_waddr && have_wdata) begin
	   maxi_bvalid <= 1;
	   have_wdata <=0; have_waddr <= 0;
	end
	
        else if (maxi_bvalid && maxi_bready) maxi_bvalid <= 0;

     end
        
   reg [15:0] ya_counter; // Change this please.
   reg [31:0] wd1;   


   reg [15:0] ksubs_control_reg;
   reg [11:0] slave_read_regno;
   reg [11:0] slave_write_regno;   


   reg [31:0] rx_Noc16_data32_hi;
   reg [31:0] rx_Noc16_data32_lo;
   reg [7:0]  rx_Noc16_data32_cmd;


   reg 	      rdstrobe;
   wire       wrstrobe = have_wdata && have_waddr;
   reg 	      rx_noc16_send; 
   wire       tx_fifo_wren;

   always @(posedge clk) if (!reset_n) begin
      ya_counter <= 0;
      wd1 <= 0;
      slave_read_regno <= 0;
   end
   else begin
      if (maxi_awvalid && maxi_awready) begin
	 slave_write_regno <= maxi_awaddr >> 2;
	 end
      rdstrobe <= (maxi_arvalid && maxi_arready);
      if (maxi_arvalid && maxi_arready) begin
	 slave_read_regno <= maxi_araddr >> 2;
	 end



      if (wrstrobe) begin
         if (slave_write_regno == 2) ksubs_control_reg <= { maxi_wstrb, maxi_wdata[27:0] };
         else if (slave_write_regno == 4) misc_reg0 <= maxi_wdata[31:0];	 
         else if (slave_write_regno == 12) rx_Noc16_data32_lo <= maxi_wdata;
         else if (slave_write_regno == 13) rx_Noc16_data32_hi <= maxi_wdata;
         else if (slave_write_regno == 14) begin rx_Noc16_data32_cmd <= maxi_wdata[7:0]; rx_noc16_send <= 1; end


	 else wd1 <= maxi_wdata[31:0];
      end
      ya_counter <= ya_counter + 1;
   end

   wire        tx_noc16_get = rdstrobe && (slave_read_regno == 9);
   
   assign pio_hwen = wrstrobe && slave_write_regno >= 16;  // (>=64 (0x40) after shift of 2 above)

   assign maxi_rresp = 2'd0; // ok code 00
  
   assign pio_addr = ((pio_hwen) ? slave_write_regno: slave_read_regno)<<2;
   assign pio_wdata = maxi_wdata;


/*
Programmer's view memory map as implemented by this susbtrate shell.
 0 ro   0x43c00000 serial no
 1      0x43c00004 counter 
 2      0x43c00008 write fold, run/stop and interrupt control in future.
 3 ro   0x43c0000c misc_mon0
 4 r/w  0x43c00010 misc_reg0
 5 ro   0x43c00014 waypoint
 6 ro   0x43c00018 result_lo
 7 ro   0x43c0001c result_hi
 8 ro   0x43c00020 tx_Noc16_lo
 9 ro   0x43c00024 tx_Noc16_hi 
10 ro   0x43c00028 tx_Noc16_status 
11 ro   0x43c0002c KsubsGpioSwitches
12 w/o  0x43c00030 rx_Noc16_lo
13 w/o  0x43c00034 rx_Noc16_hi 
14 ro   0x43c00038 rx_Noc16_status 
15      0x43c0003c <spare>
 
32  0x43c00080 user pio space available for Role use.
 
 */
   assign maxi_bid = wid;
   assign maxi_rid = rid;
   assign maxi_rlast = 1'd1;

   wire [71:0] tx_Noc16_data;
   wire        tx_Noc16_datardy;
   wire        rx_fifo_empty;
   wire        rx_fifo_full;
   wire [15:0] rx_rdcount, rx_wrcount;

   assign maxi_rdata = 
		       (slave_read_regno == 0 )? { 8'h55, design_serial_number }:
		       (slave_read_regno == 1 )? { ya_counter, ya_counter }:
		       (slave_read_regno == 2 )? { ksubsAbendSyndrome, ksubsGpioLeds, ksubs_control_reg }:
		       (slave_read_regno == 3 )? { misc_mon0 }: 
		       (slave_read_regno == 4 )? { misc_reg0 }:
		       (slave_read_regno == 5 )? ksubsManualWaypoint:
		       (slave_read_regno == 6 )? result_lo:
		       (slave_read_regno == 7 )? result_hi:		       

		       (slave_read_regno == 8 )? { tx_Noc16_data[31:0] }:
		       (slave_read_regno == 9 )? { tx_Noc16_data[63:32] }:	
		       (slave_read_regno == 10 )? { tx_Noc16_datardy, tx_Noc16_data[71:64] }:

		       (slave_read_regno == 11 )? { ksubsGpioSwitches}:		       

		       (slave_read_regno == 14 )? { rx_fifo_empty, !rx_fifo_full, 8'h00 }:
		       (slave_read_regno == 15 )? { rx_rdcount, rx_wrcount }: 
	       
		       pio_rdata;

// Slotted Ring network on chip: Noc16, FIFO interface.
// We operate the ring by programmed I/O.  This means the ARM cannot overrun since slots rotate at the rate of the software.
// This is not a performance bottleneck when there is no Role-to-Role traffic on the ring.

   // Transmit (from point-of-view of DUT) direction - data sent by the DUT to host.
   wire tx_fifo_empty;
   wire tx_fifo_full;
   wire tx_fifo_rden = tx_noc16_get;
   wire fifo_reset = ksubs_control_reg[1]; 
   assign ksubs_runstop = ksubs_control_reg[5:4]; 
   
   assign tx_fifo_wren = Ksubs3_Noc16_TxData_valid && Ksubs3_Noc16_TxData_rdy;
   assign Ksubs3_Noc16_TxData_rdy = !tx_fifo_full;
   assign tx_Noc16_datardy = !tx_fifo_empty;

  `ifdef notdef 
   FIFO_SYNC_MACRO #( 
		      .DEVICE("7SERIES"), // Target Device: "7SERIES"
		      //	.ALMOST_EMPTY_OFFSET(9'h080), // Sets the almost empty threshold
		      //	.ALMOST_FULL_OFFSET(9'h080), // Sets almost full threshold
		      .DATA_WIDTH(72), // Valid values are 1-72 (37-72 only valid when FIFO_SIZE(="36Kb") 
		      .DO_REG(0), // Optional output register (0 or 1)
		      .FIFO_SIZE ("36Kb"), // Target BRAM: "18Kb" or "36Kb" 
		      .FIRST_WORD_FALL_THROUGH("true")
		      )
   
   TX_fifo ( 
	     //.ALMOSTEMPTY(ALMOSTEMPTY), // 1-bit outputalmost empty 
	     //.ALMOSTFULL(ALMOSTFULL), // 1-bit output almost full
	     .DO(tx_Noc16_data),
	     .EMPTY(tx_fifo_empty),
	     .FULL(tx_fifo_full), 
	     //.RDCOUNT(RDCOUNT), // Output read count, width determined by FIfor depth 
	     //.RDERR(RDERR), // 1-bit output read error 
	     //.WRCOUNT(WRCOUNT), //Output write count, width determined by FIfor depth 
	     //.WRERR(WRERR), //1-bit output write error 
	     .CLK(clk), // 1-bit input clock 
	     .DI({Ksubs3_Noc16_TxData_cmd, Ksubs3_Noc16_TxData_lo}), //Input data from DUT
	     .RDEN(tx_fifo_rden),
	     .RST(fifo_reset), // 1-bit input reset 
	     .WREN(tx_fifo_wren)
	);
`endif

   // Receive (from point-of-view of DUT) direction - data sent to DUT.

   wire rx_fifo_rden;
   wire rx_fifo_wren = rx_noc16_send;
   wire [71:0] rx_Noc16_data  = { rx_Noc16_data32_cmd, rx_Noc16_data32_hi, rx_Noc16_data32_lo };

   assign rx_fifo_rden = Ksubs3_Noc16_RxData_valid && Ksubs3_Noc16_RxData_rdy;
   assign Ksubs3_Noc16_RxData_valid = !rx_fifo_empty;


   // /usr/groups/ecad/xilinx/Vivado2017/Vivado/2016.4/data/verilog/src/unimacro/FIFO_SYNC_MACRO.v


   FIFO36E1 #( 
	       //.almost_empty_offset(almost_empty_offset),
	       //.almost_full_offset(almost_full_offset),
               .DATA_WIDTH(72),
               .DO_REG(1),
               .EN_SYN("FALSE"),
	       .FIFO_MODE("FIFO36_72"),
               .FIRST_WORD_FALL_THROUGH("TRUE")
	       //.init(init),
	       //.srval(srval)
               ) 

   noc16_tx_fifo(
                 //.almostempty(almostempty),                .almostfull(almostfull), .dbiterr(),
		 .DO(tx_Noc16_data[63:0]), .DOP(tx_Noc16_data[71:64]),
                 //.eccparity(),
                 .EMPTY(tx_fifo_empty),
                 .FULL(tx_fifo_full),
                 //.rdcount(tx_rdcount),
                 //.rderr(rderr),
                 //.sbiterr(),
                 .WRCOUNT(tx_wrcount),
                 //.wrerr(wrerr),
                 .DI({Ksubs3_Noc16_TxData_lo}), .DIP(Ksubs3_Noc16_TxData_cmd),
                 //.injectdbiterr(1'b0), .injectsbiterr(1'b0),
                 .RDEN(tx_fifo_rden),
                 //.REGCE(REGCE_PATTERN),
                 .RST(fifo_reset),
                 //.RSTREG(RSTREG_PATTERN),
                 .WRCLK(clk),                  .RDCLK(clk),
                 .WREN(tx_fifo_wren)
                 );
   
   FIFO36E1 #( 
	       //.almost_empty_offset(almost_empty_offset),
	       //.almost_full_offset(almost_full_offset),
               .DATA_WIDTH(72),
               .DO_REG(1),
               .EN_SYN("FALSE"),
	       .FIFO_MODE("FIFO36_72"),
               .FIRST_WORD_FALL_THROUGH("TRUE")
	       //.init(init),
	       //.srval(srval)
               ) 

   noc16_rx_fifo (
                  //.almostempty(almostempty),                .almostfull(almostfull), .dbiterr(),
                 .DO({Ksubs3_Noc16_RxData_lo}), .DOP(Ksubs3_Noc16_RxData_cmd),
                  //.eccparity(),
                  .EMPTY(rx_fifo_empty),
                  .FULL(rx_fifo_full),
                  .RDCOUNT(rx_rdcount),
                  //.rderr(rderr),
                  //.sbiterr(),
                  .WRCOUNT(rx_wrcount),
                  //.wrerr(wrerr),
                  .DI(rx_Noc16_data[63:0]),
                  .DIP(rx_Noc16_data[71:64]),
                  //.injectdbiterr(1'b0), .injectsbiterr(1'b0),
                  .RDEN(rx_fifo_rden),
                  //.REGCE(REGCE_PATTERN),
                  .RST(fifo_reset),
                  //.RSTREG(RSTREG_PATTERN),
                  .WRCLK(clk),                  .RDCLK(clk),
                  .WREN(rx_fifo_wren)
                  );

`ifdef notdef
   FIFO_SYNC_MACRO #( 
		      .DEVICE("7SERIES"), // Target Device: "7SERIES"
		      //	.ALMOST_EMPTY_OFFSET(9'h080), // Sets the almost empty threshold
		      //	.ALMOST_FULL_OFFSET(9'h080), // Sets almost full threshold
		      .DATA_WIDTH(72), // Valid values are 1-72 (37-72 only valid when FIFO_SIZE(="36Kb") 
		      .DO_REG(0), // Optional output register (0 or 1)
		      .FIFO_SIZE ("36Kb"), // Target BRAM: "18Kb" or "36Kb" 
		      .FIRST_WORD_FALL_THROUGH("true")
		      )
   
   RX_fifo ( 
	     //.ALMOSTEMPTY(ALMOSTEMPTY), // 1-bit outputalmost empty 
	     //.ALMOSTFULL(ALMOSTFULL), // 1-bit output almost full
	     .DO({Ksubs3_Noc16_RxData_cmd, Ksubs3_Noc16_RxData_lo}), // Output data to DUT
	     .EMPTY(rx_fifo_empty),
	     .FULL(rx_fifo_full), 
	     .RDCOUNT(rx_rdcount), // Output read count, width determined by FIfor depth 
	     //.RDERR(RDERR), // 1-bit output read error 
	     .WRCOUNT(rx_wrcount), //Output write count, width determined by FIfor depth 
	     //.WRERR(WRERR), //1-bit output write error 
	     .CLK(clk),
	     .DI(rx_Noc16_data), 
	     .RDEN(rx_fifo_rden),
	     .RST(fifo_reset), // 1-bit input reset 
	     .WREN(rx_fifo_wren)
	);
`endif //  `if 0

endmodule
// EOF

