#
# cbg-hpr-ksubs4a-zynq_axi4: Makefile 
#

VIVADO=/usr/groups/ecad/xilinx/Vivado2017/Vivado/2016.4

#   $ export XILINXD_LICENSE_FILE=27020@lmserv-xilinx
#   $ export PATH=$PATH:/usr/groups/ecad/xilinx/Vivado2017/Vivado/2016.4/bin:/usr/groups/ecad/xilinx/Vivado2017/SDK/2016.4/bin

# These two libraries are needed for Kiwi-generated HLS RTL only
CVTECH1   =$(HPRLS)/hpr/cvgates.v
CVTECH2   =$(HPRLS)/hpr/cv_fparith.v

TDESTD=/tmp/cbg-hpr-ksubs4a-zynq

all:fpga


fpga:
	$(VIVADO)/bin/vivado -source hpr_zynq_axi4.tcl -mode batch 
	ls -l $(TDESTD)/topfpga.bit
	md5sum $(TDESTD)/topfpga.bit
	cp $(TDESTD)/topfpga.bit ~/Dropbox

zip:
	zip cbg_hpr_zynq_axi4.zip *.v *.tcl Makefile *.xdc hpr_zynq_axi4/*.v xilinx-ip-folder2/*.v  zynq_pinouts/*.xdc

# eof


